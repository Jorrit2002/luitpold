import os
import nltk
from exercise_one import read_file, get_file_paths
from nltk.metrics.confusionmatrix import ConfusionMatrix
from collections import Counter

def precision(tp, fp):
    """
    Correct decisions over instances assigned to class X
    True Pos / ( True Pos + False Pos )
    """
    return tp / float(tp + fp)
    #return nltk.precision(reference, test)

def recall(tp, fn):
    """
    Correct assignments to class X over all instances of class X in test set:
    True Pos / ( True Pos + False Neg)
    """
    return tp / float(tp + fn)
    #return nltk.recall(reference, test)

def f_score(precision, recall):
    """
    Combined measure of precision and recall
    ( 2 * precision * recall ) / ( precision + recall )
    """
    return 2 * (precision * recall) / float(precision + recall)
    #return nltk.f_measure(reference, test)

def annotated_found(ref, tagged):
    """
    Get
    """
    annotated = []
    tp = tn = fp = fn = 0

    for r in ref:
        for t in tagged:
            if t is not None and r is not None:
                tp += 1

            if t is None and r is None:
                tn += 1

            if t is None and r is not None:
                fn += 1

            if t is not None and r is None:
                fp += 1

    return (tp, tn, fp, fn)

def measure(classes, cm):
    """
    (0-TP, 1-TN, 2-FP, 3-FN)
    """
    true_positives = Counter()
    true_negatives = Counter()
    false_negatives = Counter()
    false_positives = Counter()

    for r in classes:
        for t in classes:
            if r == t:
                true_positives[r] += cm[r, t]
            else:
                false_negatives[r] += cm[r, t]
                false_positives[t] += cm[r, t]

    return (true_positives, true_negatives, false_positives, false_negatives)

def prepare_matrix():
    paths_by_jor = get_file_paths("{}.{}".format("en.tok.off.pos", "jor"))
    paths_by_rob = get_file_paths("{}.{}".format("en.tok.off.pos", "rob"))

    classes_jor = []
    classes_rob = []

    for i in range(min(([len(paths_by_jor), len(paths_by_rob)]))):
        file_content_jor = read_file(paths_by_jor[i])
        file_content_rob = read_file(paths_by_rob[i])
        #print(paths_by_rob[i])
        for x in range(len(file_content_jor)):

            if len(file_content_jor[x]) > 5:
                classes_jor.append(file_content_jor[x][5])
            else:
                classes_jor.append("")

            if len(file_content_rob[x]) > 5:
                classes_rob.append(file_content_rob[x][5])
            else:
                classes_rob.append("")

    return (classes_jor, classes_rob)



def main():

    #found_by_jor = annotated_class_found("{}.{}".format("en.tok.off.pos", "jor"))
    #found_by_rob = annotated_class_found("{}.{}".format("en.tok.off.pos", "rob"))

    classes = ["COU", "CIT", "NAT", "PER", "ORG", "ANI", "ENT",]# "SPO"]

    matrix_values = prepare_matrix()
    cm = ConfusionMatrix(matrix_values[0], matrix_values[1])

    m = measure(classes, cm)
    total_m = annotated_found(matrix_values[0], matrix_values[1])

    print("precision/recall/f-score for everything")
    if total_m[0] == 0:
        total_f = 0
    else:

        total_p = precision(total_m[0], total_m[2])
        total_r = recall(total_m[0], total_m[3])
        total_f = f_score(total_p, total_r)
        print("precision: {}".format(total_p))
        print("recall: {}".format(total_r))

    print("f-score: {}".format(total_f))
    print("------------------------------------------------------------------")


    for c in classes:
        print("precision/recall/f-score for class {}".format(c))

        if m[0][c] == 0:
            f = 0
        else:
            p = precision(m[0][c], m[2][c])
            r = recall(m[0][c], m[3][c])
            f = f_score(p, r)
            print("precision: {}".format(p))
            print("recall: {}".format(r))

        print("f-score: {}".format(f))
        print("------------------------------------------------------------------")


    print(cm)

if __name__ == '__main__':
    main()
