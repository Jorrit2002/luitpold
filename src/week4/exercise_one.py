import os
import nltk



def get_file_paths(file_name):

    root = os.path.dirname(os.path.abspath(__file__))
    f = []

    for path, subdirs, files in os.walk(root):
        for name in files:
            if file_name in name:
                f.append(os.path.join(path, name))
    return f

def read_file(file_path):
    content = []
    with open(file_path) as file:
        for line in file.readlines():
            content.append(line.split())
    return content

def pos_tags_to_file(file_content):
    tokenized = [list[3] for list in file_content]
    pos = nltk.pos_tag(tokenized)
    for i in range(len(pos)):
        file_content[i].append(pos[i][1])
    return file_content

def create_file(file_path, file_content):
    with open(file_path, "w") as file:
        for line in file_content:
            file.write("{}\n".format(" ".join(line)))



def main():
    file_paths = get_file_paths("en.tok.off")
    for path in file_paths:
        file_content = read_file(path)
        file_content = pos_tags_to_file(file_content)
        create_file(path+".pos", file_content)


if __name__ == "__main__":
    main()
