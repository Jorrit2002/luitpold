import os
import nltk
from exercise_one import read_file, get_file_paths, create_file


def main():
    paths_by_jor = get_file_paths("{}.{}".format("en.tok.off.pos", "jor"))
    paths_by_rob = get_file_paths("{}.{}".format("en.tok.off.pos", "rob"))


    for i in range(min(([len(paths_by_jor), len(paths_by_rob)]))):
        new_content = []
        file_content_jor = read_file(paths_by_jor[i])
        file_content_rob = read_file(paths_by_rob[i])
        for x in range(min(([len(file_content_jor), len(file_content_jor)]))):
            row = file_content_jor[x]+["", "", ""]

            if len(file_content_jor[x]) > 5:
                class_jor = file_content_jor[x][5]
            else:
                class_jor = "NONE"

            if len(file_content_rob[x]) > 5:
                class_rob = file_content_rob[x][5]
            else:
                class_rob = "NONE"

            if len(file_content_jor[x]) > 6:
                link_jor = file_content_jor[x][6]
            else:
                link_jor = "NONE"

            if len(file_content_rob[x]) > 6:
                link_rob = file_content_rob[x][6]
            else:
                link_rob = "NONE"

            print(class_jor, class_rob)


            if class_jor != class_rob:
                row[5] = "{}<>{}".format(class_jor, class_rob)
                row[0] = ">>{}".format(row[0])
            if link_jor != link_rob:
                row[6] = "{}<>{}".format(link_jor, link_rob)
                row[0] = ">>{}".format(row[0])
            new_content.append(row)

        create_file(paths_by_jor[i].replace("jor", "ent"), new_content)


if __name__ == '__main__':
    main()
