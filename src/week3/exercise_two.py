import sys
import os
import nltk
from nltk.tag.stanford import StanfordNERTagger
from nltk.corpus import wordnet as wn


def exercise_one():
    print("1)")
    file_content = open('ada_lovelace.txt').read()


    st = StanfordNERTagger('../stanford/classifiers/english.all.3class.distsim.crf.ser.gz', '../stanford/stanford-ner-3.4.jar')
    tag_list = st.tag(file_content.split())

    persons = [word for (word, tag) in tag_list if tag == "PERSON"]
    organizations = [word for (word, tag) in tag_list if tag == "ORGANIZATION"]
    locations = [word for (word, tag) in tag_list if tag == "LOCATION"]

    print(tag_list)

    print("persons: \n{}".format(persons))
    print("organizations: \n{}".format(organizations))
    print("locations: \n{}".format(locations))
    print("\n\n")

    print("1) Count how many Persons, Organizations, \and Locations you find")
    print("Number of persons: {}".format(len(persons)))
    print("Number of organizations: {}".format(len(organizations)))
    print("Number of locations: {}".format(len(locations)))


def exercise_two():
    print("2)")
    file_content = open('ada_lovelace.txt').read()

    taggers = [("English all 3 class", "english.all.3class.distsim.crf.ser.gz"),
               ("English conll 4 class", "english.conll.4class.distsim.crf.ser.gz"),
               ("English muc 7 class", "english.muc.7class.distsim.crf.ser.gz"),
               ("English nowiki 3 class", "english.nowiki.3class.distsim.crf.ser.gz"),
               ]

    for tagger in taggers:

        st = StanfordNERTagger('../stanford/classifiers/'+tagger[1], '../stanford/stanford-ner-3.4.jar')
        tag_list = st.tag(file_content.split())

        print(tagger[0])
        print(tag_list)

        tags = []
        for word, tag in tag_list:
            if tag not in tags:
                tags.append(tag)

        print("{} - {} tags: {}".format(tagger[0], len(tags), tags))


def exercise_three():
    print("3)")
    file_content = open('ada_lovelace.txt').read()
    word_list = nltk.word_tokenize(file_content)
    noun_list = []

    for word in word_list:
        synsets = wn.synsets(word, pos="n")
        if len(synsets) > 0:
            noun_list.append(word)

    st = StanfordNERTagger('../stanford/classifiers/english.muc.7class.distsim.crf.ser.gz', '../stanford/stanford-ner-3.4.jar')
    tag_list = st.tag(noun_list)

    for (word, tag) in tag_list:
        print("{:<20}{}".format(word,tag))

def main():
    exercise_one()
    exercise_two()
    exercise_three()

if __name__ == "__main__":
    main()
