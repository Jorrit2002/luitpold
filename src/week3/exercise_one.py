"""
Exercise 1 - Week 3
"""
import nltk
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from collections import defaultdict

def hyponym_of(ss1, ss2):
    """
    Returns True if ss1 is a hyponym of ss2
    otherwise it's False
    """
    if ss1 == ss2:
        return True

    for hyponym in ss1.hyponyms():
        if ss2 == hyponym:
            return True

        if hyponym_of(hyponym, ss2):
            return True

    return False

def hypernym_of(ss1, ss2):
    """
    Returns True if ss1 is a hypernym of ss2
    otherwise it's False
    """
    if ss1 == ss2:
        return True

    for hypernym in ss1.hypernyms():
        if ss2 == hypernym:
            return True

        if hypernym_of(hypernym, ss2):
            return True

    return False

def words_that_refer_to(word, text):
    synsets = wn.synsets(word)
    words = []
    lemmatizer = WordNetLemmatizer()
    for w in text:
        current_word_synsets = wn.synsets(lemmatizer.lemmatize(w))
        for synset in synsets:
            for ss in current_word_synsets:
                if hyponym_of(synset, ss):
                    words.append(w)


    return words

def top_twenty_five():
    return ["act", "animal", "artifact",
            "attribute", "body", "cognition",
            "communication", "event", "feeling",
            "food", "group", "location",
            "motive", "natural object", "natural phenomenon",
            "person", "plant", "possession",
            "process", "quantity", "relation",
            "shape", "state", "substance",
            "time"]


def assignment_one():
    """
    assignment one
    """
    text = open('ada_lovelace.txt').read()
    tokens = nltk.word_tokenize(text)

    refer_to_relative = words_that_refer_to("relative", tokens)
    refer_to_illness = words_that_refer_to("illness", tokens)
    refer_to_science = words_that_refer_to("science", tokens)
    print("1.a) There are {} number of words which refer to 'relative'".format(len(refer_to_relative)))
    print("1.b) There are {} number of words which refer to 'illness'".format(len(refer_to_illness)))
    print("1.c) There are {} number of words which refer to 'science'".format(len(refer_to_science)))

def assignment_two():
    """
    assignment two
    """
    text = open('ada_lovelace.txt').read()
    tokens = nltk.word_tokenize(text)
    tags = nltk.pos_tag(tokens)

    lemma_nouns = []
    one_hypernym = []
    lemmatizer = WordNetLemmatizer()

    for word, pos in tags:
        if pos in ["NN", "NNS", "NNP", "NNPS"]:
            synsets = wn.synsets(lemmatizer.lemmatize(word))
            if len(synsets) == 1:
                one_hypernym.append(synsets)

            lemma_nouns += synsets


    hypernyms = defaultdict(list)
    multiple_hypernyms = []
    total_amount_of_hypernyms = 0
    for word in top_twenty_five():
        synsets = wn.synsets(word, pos="n")
        for synset in synsets:
            for noun_synset in lemma_nouns: # These are already synsets
                if len(noun_synset.hypernyms()) > 1:
                    multiple_hypernyms.append(noun_synset)

                if hypernym_of(synset, noun_synset):
                    hypernyms[word].append(noun_synset)

    for noun_synset in lemma_nouns: # These are already synsets
        total_amount_of_hypernyms += len(noun_synset.hypernyms())

    print("2.a) Amount of words with one hypernyms {}. Examples: {}"
           .format(len(one_hypernym),
           [[synset.name() for synset in synsets] for synsets in one_hypernym[0:2]]))
    print("2.b) Amount of times we had to choose between more than one hypernym was {} Examples: {}"
          .format(len(multiple_hypernyms), [[synset.name(), synset.hypernyms()] for synset in multiple_hypernyms[0:2]]))
    print("2.c) Average amount of hypernyms was {}"
          .format(total_amount_of_hypernyms / len(lemma_nouns)))

def assignment_three():
    """
    assignment three
    """
    word_pairs = [("car", "automobile"), ("coast", "shore"), ("food", "fruit"), ("journey", "car"), ("monk", "slave"), ("moon", "shire")]
    article_scores = [3.92, 3.70, 3.08, 1.16, 0.55, 0.08]
    scores_wup = []
    scores_lch = []
    for (word1, word2) in word_pairs:
        wordFromList1 = wn.synsets(word1)
        wordFromList2 = wn.synsets(word2)
        if wordFromList1 and wordFromList2:
            lch_s = wordFromList1[0].lch_similarity(wordFromList2[0])
            scores_lch.append((lch_s, (word1, word2)))

            wup_s = wordFromList1[0].wup_similarity(wordFromList2[0])
            scores_wup.append((wup_s, (word1, word2)))


    print("3) WordNet Similarity")
    print("{:<20}{:<20}".format("Word pair", "WordNet score"))
    for (name, scores) in [("wup" , scores_wup), ("lch" ,scores_lch)]:
        print("\n{}".format(name))
        scores_sorted = sorted(scores, key=lambda  tup : tup[0], reverse=True)
        for item in scores_sorted:
            print("{:<20}{}".format(item[1][0]+"-"+item[1][1], item[0]))


def main():
    assignment_one()
    print()
    assignment_two()
    print()
    assignment_three()

if __name__ == '__main__':
    main()
