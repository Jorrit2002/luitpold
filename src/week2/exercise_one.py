import nltk
from nltk.collocations import *
from nltk.metrics.spearman import *

def get_ngrams(list, n):

    output = []
    for i in range(len(list)-n+1):
        output.append(list[i:i+n])
    result = []
    for ngram in output:
        result.append(' '.join(ngram))
    return result


def sorted_freq(text, n=1, sort_index=0, asc=True):
    """returns a freq dist list on alphabettically order"""
    text = get_ngrams(text, n)

    fdist = nltk.FreqDist(text)
    fdist_sorted = sorted(fdist.items(), key=lambda tup : tup[sort_index], reverse=not asc)


    return fdist_sorted

def bigram_collocations(tokens, number=20, type="raw"):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokens)
    if type == "pmi":
        list = finder.nbest(bigram_measures.pmi, number)
    elif type == "chi_sq":
        list = finder.nbest(bigram_measures.chi_sq, number)
    else:
        list = finder.nbest(bigram_measures.raw_freq, number)
    return list


def exercise_one():
    text = open('holmes.txt').read()
    tokens = nltk.wordpunct_tokenize(text)

    character_types_unigrams = sorted_freq(text, 1)
    word_types_unigrams = sorted_freq(tokens, 1)

    """
    1a) how many character types are found, followed by the character type list, alphabetically
    ordered.
    """
    print("Number of character types: {}".format(len(character_types_unigrams)))
    for ct in character_types_unigrams:
        print("{:<20}".format(ct[0]))


    """
    1b) how many word types are found, followed by the word type list, alphabetically
    ordered
    """
    print("Number of word types: {}".format(len(word_types_unigrams)))
    for wt in word_types_unigrams:
        print("{}".format(wt[0]))


    """
    1c) the top 20 character-level unigrams, bigrams, and trigrams, ordered by descending
    frequency (from the most frequent to the least). (What could this information
    be useful for?)
    """

    print("\n top 20 character-level unigrams:")
    for ct in sorted_freq(text, 1, 1, False)[:20]:
        print("{:<20} {}".format(ct[0], ct[1]))

    print("\n top 20 character-level bigrams:")
    for ct in sorted_freq(text, 2, 1, False)[:20]:
        print("{:<20} {}".format(ct[0], ct[1]))

    print("\n top 20 character-level trigrams:")
    for ct in sorted_freq(text, 3, 1, False)[:20]:
        print("{:<20} {}".format(ct[0], ct[1]))

    """
    1d) the top 20 word-level unigrams, bigrams and trigrams, ordered likewise
    """

    print("\n top 20 word-level unigrams:")
    for wt in sorted_freq(tokens, 1, 1, False)[:20]:
        print("{:<20} {}".format(wt[0], wt[1]))

    print("\n top 20 word-level bigrams:")
    for wt in sorted_freq(tokens, 2, 1, False)[:20]:
        print("{:<20} {}".format(wt[0], wt[1]))

    print("\n top 20 word-level trigrams:")
    for wt in sorted_freq(tokens, 3, 1, False)[:20]:
        print("{:<20} {}".format(wt[0], wt[1]))


    """
    2a) the most likely 20 collocations using PMI
    """
    print("\n most likely 20 collocation using PMI:")
    pmi_scored = bigram_collocations(tokens, 20, "pmi")
    for bigram in pmi_scored:
        print(bigram[0], bigram[1])


    """
    2b) the most likely 20 collocations using Chi-square
    """
    print("\n most likely 20 collocation using Chi-square:")
    chi_scored = bigram_collocations(tokens, 20, "chi_sq")
    for bigram in chi_scored:
        print(bigram[0], bigram[1])


    """
    2c) just looking at them, there are surely differences in the rankings. Can you provide
    an explanation? You can also compare what you get to what you got just by taking
    the top 20 bigrams (without any association measure — raw freq, see also code
    below)
    """

    print("\n most likely 20 collocation using raw frequencies:")
    raw_scored = bigram_collocations(tokens, 20, "raw")
    for bigram in raw_scored:
        print(bigram[0], bigram[1])




    """
    Output PMI:
    1 /
    31 Lyon
    ARTHUR CONAN
    Alicia Whittington
    Amateur Mendicant
    Amoy River
    Anderson ",
    Apache Indians
    Arabian Nights
    Arnsworth Castle
    Atkinson brothers
    BERYL CORONET
    BLUE CARBUNCLE
    BOSCOMBE VALLEY
    Bengal Artillery
    Bermuda Dockyard
    Blue Carbuncle
    Botany variable
    CONAN DOYLE
    COPPER BEECHES

    Output Chi-square:
    1 /
    31 Lyon
    ARTHUR CONAN
    Alicia Whittington
    Amateur Mendicant
    Amoy River
    Anderson ",
    Apache Indians
    Arabian Nights
    Arnsworth Castle
    Atkinson brothers
    BERYL CORONET
    BLUE CARBUNCLE
    BOSCOMBE VALLEY
    Bengal Artillery
    Bermuda Dockyard
    Beryl Coronet
    Blue Carbuncle
    Botany variable
    Briony Lodge
    """


    """
    2d) extra: calculate the Spearman’s coefficient on the different collocation ranks
    (item 6 at link given in “hint” box here below)
    """
    spearman_score = spearman_correlation(ranks_from_sequence(pmi_scored),ranks_from_sequence(chi_scored))
    print("\nSpearman correlation: {}".format(spearman_score))


def main():
    exercise_one()


if __name__ == "__main__":
    main()