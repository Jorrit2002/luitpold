import sys
import nltk
import collections
from nltk.corpus import gutenberg
from nltk.corpus import brown
from nltk import FreqDist, NgramTagger
from nltk.collocations import *
from nltk.metrics.spearman import *
from nltk.tag import StanfordPOSTagger

from exercise_one import *

def holmes_tagger():
    text = nltk.word_tokenize(open("holmes.txt").read())
    return nltk.pos_tag(text)


def find_sentence_with_pos_and_word(p, w, sentences):
    s = []
    for sentence in sentences:
        for word, pos in sentence:

            if word != w:
                continue
            if pos not in p:
                continue

            p.pop(p.index(pos))
            s.append(sentence)

    return s

def most_frequent_pos_preceding(word, pos, sentences):
    c = collections.Counter()

    for sentence in sentences:
        for i in range(1, len(sentence)):
            if word != sentence[i][0]:
                continue

            if pos == sentence[i][1]:
                c[sentence[i-1][1]] += 1


    return c


def most_frequent_pos_following(word, pos, sentences):
    c = collections.Counter()

    for sentence in sentences:
        for i in range(1, len(sentence)-1):
            if word != sentence[i][0]:
                continue

            if pos == sentence[i][1]:
                c[sentence[i+1][1]] += 1


    return c

def exercise_two():
    br_tw = nltk.corpus.brown.tagged_words(categories='adventure', tagset='universal')
    br_ts = nltk.corpus.brown.tagged_sents(categories='adventure', tagset='universal')
    #print(br_ts)
    word_tag_fd =  nltk.FreqDist(br_tw)

    different_pos_tags = nltk.FreqDist(tag for (word, tag) in br_tw)
    different_words = nltk.FreqDist(word for (word, tag) in br_tw)

    pos_tags_20th_sent = nltk.FreqDist(tag for (word, tag) in br_ts[19])
    pos_tags_40th_sent = nltk.FreqDist(tag for (word, tag) in br_ts[39])

    adv = nltk.FreqDist(word for (word, tag) in br_tw if tag == "ADV")
    adj = nltk.FreqDist(word for (word, tag) in br_tw if tag == "ADJ")

    word_so = nltk.FreqDist(tag for (word, tag) in br_tw if word.lower() == "so")
    sentences_with_so = find_sentence_with_pos_and_word(list(word_so.keys()), "so", br_ts)

    print("2.a) Words: {}".format(len(br_tw)))
    print("2.a) Sentences: {}".format(len(br_ts)))
    print("2.b) 50th word: {}".format(br_tw[49]))
    print("2.b) 75th word: {}".format(br_tw[74]))
    print("2.c) Different kinds of POS: {}".format(len(different_pos_tags)))
    print("2.d) Top 15 word and counts: {}".format(different_words.most_common(15)))
    print("2.e) Top 15 POS tag and counts: {}".format(different_pos_tags.most_common(15)))
    print("2.f) Most frequent POS tag 20th sentence: {}".format(pos_tags_20th_sent.most_common()))
    print("2.f) Most frequent POS tag 40th sentence: {}".format(pos_tags_40th_sent.most_common()))
    print("2.g) Most frequent adverb: {}".format(adv.most_common(1)))
    print("2.h) Most frequent adjective: {}".format(adj.most_common(1)))
    print("2.i) POS of \"so\": {}".format(list(word_so.keys())))
    print("2.j) Most frequent POS of \"so\": {}".format(word_so.most_common(1)))
    print("2.k) Example sentence: {}".format(sentences_with_so))

    for pos in word_so.keys():
        print("2.l) Most likely {} POS preceding: {}".format(pos, most_frequent_pos_preceding("so", pos, br_ts).most_common(1)))
        print("2.l) Most likely {} POS following: {}".format(pos, most_frequent_pos_following("so", pos, br_ts).most_common(1)))

    holmes_pos = holmes_tagger()
    print("3) {}".format(holmes_pos))


    holmes_pos_list = [tag for (word, tag) in holmes_pos]
    pmi_colloc = bigram_collocations(holmes_pos_list, 20, "pmi")
    chi_colloc = bigram_collocations(holmes_pos_list, 20, "chi_sq")
    raw_colloc = bigram_collocations(holmes_pos_list, 20, "raw")
    print("4) can you get collocations for POS tags?")
    print("PMI collocations: \n {}". format(pmi_colloc))
    print("Chi square collocations: \n {}". format(chi_colloc))
    print("Raw collocations: \n {}". format(raw_colloc))





if __name__ == '__main__':
    exercise_two()
