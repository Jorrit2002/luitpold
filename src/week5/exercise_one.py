import wikipedia
import nltk
from nltk.corpus import wordnet as wn
from nltk.wsd import lesk
from collections import defaultdict

def main():
    subjects = ["koala", "Cardiac surgery", "tire", "asphalt", "Kinder Bueno"]
    total_poly_synsets = [0, 0]
    synset_counter = defaultdict(int)
    different_senses_subjects = []
    random_cases = []

    for subject in subjects:
        poly_counter = 0
        different_senses = defaultdict(list)
        words_tagged_different = defaultdict(list)
        content = wikipedia.summary(subject)
        tokens = nltk.wordpunct_tokenize(content)
        words = [word for (word, tag) in nltk.pos_tag(tokens) if tag in ["NN", "NNP", "NNPS", "NNS"]]
        for word in words:
            synsets = wn.synsets(word, pos="n")
            if len(synsets) > 1:
                poly_counter += 1
                total_poly_synsets[0] += len(synsets)
                total_poly_synsets[1] += 1
                synset_counter[len(synsets)] += 1
                lesk_synset = lesk(content, word, "n")
                if lesk_synset.name() not in words_tagged_different[word]:
                    words_tagged_different[word].append(lesk_synset.name())

                if len(random_cases) < 7:
                    random_cases.append((word, content, lesk_synset))

        different_senses_subjects.append(words_tagged_different)

        print("1) Number of polysemius words for subject {}: {} out of {}".format(subject, poly_counter, len(words)))

    print("3) The avarage number of synsets per polysemius word: {}".format(total_poly_synsets[0]/total_poly_synsets[1]))
    print("4)\n{:<20}{}".format("Number of synsets", "freqency"))
    for item in sorted(synset_counter.items(), key=lambda tup : tup[0]):
        print("{:<20}{}".format(item[0], item[1]))

    #print("5) Random selected cases")
    #for case in random_cases:
        #print("{}\n\n{}\n\n{}".format(case[0], case[1], case[2].definition()))
        #print("----------------------------------")

    print(different_senses_subjects)
    for subject_list in different_senses_subjects:
        for word, synsets in subject_list.items():
            if len(synsets) > 1:
                print("6) There are words with ")
                break


if __name__ == "__main__":
    main()
