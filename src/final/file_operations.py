"""
All function related to file operations
"""
import os
import sys

def get_file_paths(file_name):

    root = os.path.dirname(os.path.abspath(__file__))
    f = []

    for path, subdirs, files in os.walk(root):
        for name in files:
            if file_name == name:
                fp = os.path.join(path, name)
                if "test" in fp:
                    f.append(fp)
    return f

def read_file(file_path):
    content = []
    with open(file_path) as file:
        for line in file.readlines():
            content.append(line.split())

    return content

def create_file(file_path, file_content):
    with open(file_path, "w") as file:
        for line in file_content:
            file.write("{}\n".format(" ".join(line)))
