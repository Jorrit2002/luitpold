import os
import nltk
from file_operations import read_file, get_file_paths, create_file
import sys

def main(argv):
    if argv[1] != "":
        ent = argv[1]
    else:
        ent = None
    paths_by_annotated = get_file_paths("{}.{}".format("en.tok.off.pos", "ent"))
    paths_by_output = get_file_paths("{}.{}".format("en.tok.off.pos", "output"))


    for i in range(min(([len(paths_by_annotated), len(paths_by_output)]))):
        new_content = []
        file_content_annotated = read_file(paths_by_annotated[i])
        file_content_output = read_file(paths_by_output[i])
        print("\n"+paths_by_output[i])
        print("{:<20} {:<20} {:<20}".format("word", "annotated", "output"))
        print("------------------------------------------------------------")
        if len(file_content_annotated) == len(file_content_output):
            for x in range(min(([len(file_content_annotated), len(file_content_annotated)]))):
                row = file_content_annotated[x]+["", "", ""]

                if len(file_content_annotated[x]) > 5:
                    class_annotated = file_content_annotated[x][5]
                else:
                    class_annotated = "NONE"

                if len(file_content_output[x]) > 5:
                    class_output = file_content_output[x][5]
                else:
                    class_output = "NONE"

                if len(file_content_annotated[x]) > 6:
                    link_annotated = file_content_annotated[x][6]
                else:
                    link_annotated = "NONE"

                if len(file_content_output[x]) > 6:
                    link_output = file_content_output[x][6]
                else:
                    link_output = "NONE"


                if class_annotated != class_output:
                    if ent == None or (class_annotated == ent or class_output == ent):
                        print("{:<20} {:<20} {:<20}".format(file_content_annotated[x][3], class_annotated, class_output))




if __name__ == '__main__':
    main(sys.argv)
