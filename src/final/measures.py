import os
import nltk
from file_operations import read_file, get_file_paths
from nltk.metrics.confusionmatrix import ConfusionMatrix
from collections import Counter

def precision(tp, fp):
    """
    Correct decisions over instances assigned to class X
    True Pos / ( True Pos + False Pos )
    """
    return tp / float(tp + fp)
    #return nltk.precision(reference, test)

def recall(tp, fn):
    """
    Correct assignments to class X over all instances of class X in test set:
    True Pos / ( True Pos + False Neg)
    """
    return tp / float(tp + fn)
    #return nltk.recall(reference, test)

def f_score(precision, recall):
    """
    Combined measure of precision and recall
    ( 2 * precision * recall ) / ( precision + recall )
    """
    return 2 * (precision * recall) / float(precision + recall)
    #return nltk.f_measure(reference, test)

def annotated_found(ref, tagged):
    """
    Get
    """
    annotated = []
    tp = 0
    tn = 0
    fp = 0
    fn = 0

    # print(ref)
    # print(tagged)

    for i in range(len(ref)):
        r = ref[i]
        t = tagged[i]

        if t != "" and r == t:
            tp += 1
        if t != r and t != "":
            fp += 1
        if t == "" and r == t:
            tn += 1
        if t != r and t == "":
            fn += 1

    # for r in ref:
    #     for t in tagged:
    #         #print(r, t)
    #     if t != "" and r != "":
    #         tp += 1
    #
    #     if t == "" and r == "":
    #         tn += 1
    #
    #     if t == "" and r != "":
    #         fn += 1
    #
    #     if t != "" and r == "":
    #         fp += 1

    return (tp, tn, fp, fn)

def measure(classes, cm):
    """
    (0-TP, 1-TN, 2-FP, 3-FN)
    """
    true_positives = Counter()
    true_negatives = Counter()
    false_negatives = Counter()
    false_positives = Counter()

    for r in classes:
        for t in classes:
            if r == t:
                true_positives[r] += cm[r, t]
            else:
                false_negatives[r] += cm[r, t]
                false_positives[t] += cm[r, t]

    return (true_positives, true_negatives, false_positives, false_negatives)

def prepare_matrix():
    paths_annotated = get_file_paths("{}.{}".format("en.tok.off.pos", "ent"))
    paths_output = get_file_paths("{}.{}".format("en.tok.off.pos", "output"))

    classes_annotated = []
    classes_output = []

    for i in range(min(([len(paths_annotated), len(paths_output)]))):
        file_content_annotated = read_file(paths_annotated[i])
        file_content_output = read_file(paths_output[i])
        # print("-------------")
        # print(paths_output[i])

        if len(file_content_annotated) == len(file_content_output):
            for x in range(len(file_content_annotated)):

                if len(file_content_annotated[x]) > 5:
                    if file_content_annotated[x][5]  not in ["0"]:
                        classes_annotated.append(file_content_annotated[x][5])
                    else:
                        classes_annotated.append("")
                else:
                    classes_annotated.append("")

                # print(paths_output[i])
                # print(file_content_output[-1])
                #print(len(file_content_output), len(file_content_annotated))
                # print(x)
                # print(len(file_content_output[x]))

                if len(file_content_output[x]) > 5:
                    classes_output.append(file_content_output[x][5])
                else:
                    classes_output.append("")

    return (classes_annotated, classes_output)


def url_matrix():
    paths_annotated = get_file_paths("{}.{}".format("en.tok.off.pos", "ent"))
    paths_output = get_file_paths("{}.{}".format("en.tok.off.pos", "output"))

    classes_annotated = []
    classes_output = []
    for i in range(min(([len(paths_annotated), len(paths_output)]))):
        file_content_annotated = read_file(paths_annotated[i])
        file_content_output = read_file(paths_output[i])

        if len(file_content_annotated) == len(file_content_output):
            for x in range(len(file_content_annotated)):

                if len(file_content_annotated[x]) > 6:
                    classes_annotated.append(file_content_annotated[x][6])
                else:
                    classes_annotated.append("")

                if len(file_content_output[x]) > 6:
                    classes_output.append(file_content_output[x][6])
                else:
                    classes_output.append("")

    return (classes_annotated, classes_output)

def main():

    #found_by_jor = annotated_class_found("{}.{}".format("en.tok.off.pos", "jor"))
    #found_by_rob = annotated_class_found("{}.{}".format("en.tok.off.pos", "rob"))

    # classes = ["COU", "CIT", "NAT", "PER", "ORG","ENT",]# "SPO"]

    matrix_values = prepare_matrix()
    classes = list(set([row for row in matrix_values[0]+matrix_values[1]]))
    #print(classes)
    cm = ConfusionMatrix(matrix_values[0], matrix_values[1])

    m = measure(classes, cm)
    total_m = annotated_found(matrix_values[0], matrix_values[1])
    print(total_m)
    print("precision/recall/f-score for everything")
    if total_m[0] == 0:
        total_f = 0
    else:

        total_p = precision(total_m[0], total_m[2])
        total_r = recall(total_m[0], total_m[3])
        total_f = f_score(total_p, total_r)
        print("precision: {}".format(total_p))
        print("recall: {}".format(total_r))

    print("f-score: {}".format(total_f))
    print("------------------------------------------------------------------")


    for c in classes:
        print("precision/recall/f-score for class {}".format(c))

        if m[0][c] == 0:
            f = 0
        else:
            p = precision(m[0][c], m[2][c])
            r = recall(m[0][c], m[3][c])
            f = f_score(p, r)
            print("precision: {}".format(p))
            print("recall: {}".format(r))

        print("f-score: {}".format(f))
        print("------------------------------------------------------------------")


    print(cm)

    print("------------------------------------------------------------------")
    print("------------------------------------------------------------------")

    urls_values = url_matrix()
    urls_annotated = annotated_found(urls_values[0], urls_values[1])

    correct = incorrect = 0

    # for x in range(len(urls_values[0])):
    #     if urls_values[0][x] == urls_values[1][x]:
    #         correct += 1
    #     else:
    #         incorrect += 1
    # print("accuracy: {}".format(correct/(correct+incorrect)))
    # print("correct: {} \n incorrect: {}".format(correct, incorrect))

    print("precision/recall/f-score for URLs")
    if urls_annotated[0] == 0:
        urls_f = 0
    else:
        urls_p = precision(urls_annotated[0], urls_annotated[2])
        urls_r = recall(urls_annotated[0], urls_annotated[3])
        urls_f = f_score(urls_p, urls_r)

        print("precision: {}".format(urls_p))
        print("recall: {}".format(urls_r))

    print("f-score: {}".format(urls_f))


if __name__ == '__main__':
    main()
