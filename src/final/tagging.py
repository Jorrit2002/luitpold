"""
All functions related to tagging
"""
import sys
import nltk
import wikipedia
from nltk.tag.stanford import StanfordNERTagger
from nltk.corpus import wordnet as wn
from nltk.wsd import lesk
from nltk.stem import WordNetLemmatizer
from nltk.collocations import *
import file_operations as fo
from nltk.stem.snowball import EnglishStemmer
from nltk.text import TextCollection
# def wikipedia_exception(pages):
#     title = pages.pop()
#     try:
#         if len(pages) > 0:
#             return wikipedia.page(title=title)+wikipedia_exception(list(pages))
#         else:
#             return wikipedia.page(title=title)
#     except wikipedia.exceptions.DisambiguationError as e:
#         print(e.options)
#         values = list(e.options.remove(title))
#         return wikipedia_exception(values)


categories = [
    ("PER", wn.synset("person.n.01")),
    ("CIT", wn.synset("city.n.01")),
    ("COU", wn.synset("country.n.01")),
    ("COU", wn.synset("country.n.02")),
    ("COU", wn.synset("state.n.01")),
    ("COU", wn.synset("region.n.03")),
    ("NAT", wn.synset("location.n.01")),
    ("NAT", wn.synset("land.n.04")),
    ("ANI", wn.synset("animal.n.01")),
    ("ORG", wn.synset("organization.n.01")),
    ("SPO", wn.synset("sport.n.01")),
    ("ENT", wn.synset("entertainment.n.01"))]

# def wikipedia_tfidf_extraction(word, file_content):
#     pages = []
#     wikipedia.set_lang("en")
#     try:
#         page = wikipedia.page(word)
#         return page
#     except wikipedia.exceptions.DisambiguationError as e:
#         for page in e.options:
#             try:
#                 pages.append(wikipedia.page(title=page))
#             except wikipedia.exceptions.DisambiguationError as e:
#                 continue
#             except wikipedia.exceptions.PageError as e:
#                 continue
#             except wikipedia.exceptions.WikipediaException as e:
#                 continue
#     except wikipedia.exceptions.PageError as e:
#         #not a wikipedia page found
#         return None
#     except wikipedia.exceptions.WikipediaException as e:
#         return None
#
#
#     text_collection = TextCollection([page.summary.lower() for page in pages])
#     best_score = 0
#     best_page = None
#
#     for page in pages:
#         score = 0
#         for row in file_content:
#             if row[4] in ["NN", "NNP", "NNPS", "NNS"]:
#                 score += text_collection.tf_idf(row[3], page.summary.lower())
#         if score > best_score:
#             best_score = score
#             best_page = page
#
#     return best_page


def entity_to_summery(page):
    tokens = nltk.wordpunct_tokenize(page.summary)
    tagged = nltk.pos_tag(tokens)
    words = nltk.FreqDist([word for (word, tag) in tagged if tag in ["NN", "NNP", "NNPS", "NNS"]])
    for (word, freq) in words.most_common(5):
        possible_cats = []
        synset = lesk(tokens, word, pos="n")
        if synset is None:
            # print("NOnE")
            continue

        for (cat, category) in categories:
            if hypernym_of(synset, category):
                possible_cats.append((cat, category))

        if len(possible_cats) > 0:
            return most_similar_synset(synset, possible_cats)
    return None

def most_similar_synset(search, ss_list):
    """ From a list get the synset with the lowest distance to the synset"""
    lowest = -1;
    highest_cat = None
    for cat, synset in ss_list:
        current = wn.path_similarity(search, synset)
        if current > lowest:
            lowest = current
            highest_cat = cat

    return highest_cat

def hypernym_of(ss1, ss2, print_hypernym=False):
    """
    Returns True if ss1 is a hypernym of ss2
    otherwise it's False
    """

    if ss1 == ss2:
        return True

    for hypernym in ss1.hypernyms() + ss1.instance_hypernyms():
        if print_hypernym is True:
            print(hypernym)

        if ss2 == hypernym:
            return True

        if hypernym_of(hypernym, ss2):
            return True

    return False

def content_to_word_list(file_content):
    return [list[3] for list in file_content]

def pos_tags_to_content(word_list):
    """
    returns a list of the file content with the POS tags added

    returns a list with all the pos tags
    """
    return nltk.pos_tag(word_list)

def nert_to_content(word_list):
    taggers = [("English all 3 class", "english.all.3class.distsim.crf.ser.gz"),
               ("English conll 4 class", "english.conll.4class.distsim.crf.ser.gz"),
               ("English muc 7 class", "english.muc.7class.distsim.crf.ser.gz"),
               ("English nowiki 3 class", "english.nowiki.3class.distsim.crf.ser.gz"),
              ]

    st = StanfordNERTagger("stanford/classifiers/"+taggers[0][1], 'stanford/stanford-ner-3.4.jar')
    return st.tag(word_list)

def wn_entity_to_content(word, word_list):
    """
    return a string
    """

    # print(word)
    possible_cats = []
    synset = lesk(word_list, word, pos="n")
    if synset is None:
        return None
    else:
        for (cat, category) in categories:
            if hypernym_of(synset, category):
                possible_cats.append((cat, category))

        cat = most_similar_synset(synset, possible_cats)
        if cat is not None:
            return cat

    return None

def bigram_collocations(tokens, number=20, type="raw"):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokens)
    if type == "pmi":
        list = finder.nbest(bigram_measures.pmi, number)
    elif type == "chi_sq":
        list = finder.nbest(bigram_measures.chi_sq, number)
    else:
        list = finder.nbest(bigram_measures.raw_freq, number)
    return list

def trigram_collocations(tokens, number=20, type="raw"):
    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    finder = TrigramCollocationFinder.from_words(tokens)
    if type == "pmi":
        list = finder.nbest(trigram_measures.pmi, number)
    elif type == "chi_sq":
        list = finder.nbest(trigram_measures.chi_sq, number)
    else:
        list = finder.nbest(trigram_measures.raw_freq, number)
    return list


def wikipedia_url(ngram, word_list):

    word = " ".join(ngram)

    wikipedia.set_lang("en")

    try:
        page = wikipedia.page(word)
        return page.url
    except wikipedia.exceptions.DisambiguationError as e:
        synset = lesk(word_list, word, pos="n")
        if synset is None:
            for title in e.options:
                try:
                    page = wikipedia.page(title=title)
                    return page.url
                except wikipedia.exceptions.DisambiguationError as e:
                    continue
                except wikipedia.exceptions.PageError as e:
                    continue
        else:
            pages = []
            for page in e.options:
                try:
                    pages.append(wikipedia.page(title=page))
                except wikipedia.exceptions.DisambiguationError as e:
                    continue
                except wikipedia.exceptions.PageError as e:
                    continue
            for page in pages:
                if lesk(page.content, word) == synset:
                    return page.url
    except wikipedia.exceptions.PageError as e:
        #not a wikipedia page found
        return ""
    return ""

def key_finder(word_list, entity_list):

    chi_scored_bigrams = bigram_collocations(word_list, 10, "chi_sq")
    chi_scored_trigrams = trigram_collocations(word_list, 5, "chi_sq")

    keyword_list = []

    i = 0
    while i < len(word_list):
        word = word_list[i]
        entity = entity_list[i]

        if i < len(word_list)-1: # and len(file_content[(i+1)]) > 5:
        # if entity == entity_list[(i+1)]:
            bigram = is_ngram(chi_scored_bigrams, word)
            if bigram is not None:
                if i < len(word_list)-2: #and len(file_content[(i+2)]) > 5:
                    # if entity == entity_list[(i+2)]:
                    trigram = is_ngram(chi_scored_trigrams, word)
                    if trigram is not None:
                        keyword_list.append(trigram)
                        keyword_list.append(trigram)
                        keyword_list.append(trigram)
                        i += 3
                        continue
                keyword_list.append(bigram)
                keyword_list.append(bigram)
                i += 2
                continue
        keyword_list.append((word,))

        i += 1
    return keyword_list

def is_ngram(ngrams, word):
    for ngram in ngrams:
        if word == ngram[0]:
            return ngram
    return None




def hypernym_test(argv):
    if argv[1] != "" and argv[2] != "":
        word = argv[1]
        fp = argv[2]
        file_content = fo.read_file(fp)
        text = content_to_word_list(file_content)
        synset = lesk(text, word, "n")

        print("\n\n"+" ".join(text)+"\n\n")
        print("lesk: {}".format(synset))
    else:
        synset = wn.synset(argv[1]+".n.01")

    for cat in categories:
        if hypernym_of(synset, cat[1], print_hypernym=True):
            print("hypernyM!: {}".format(cat[0]))
            break

def wiki_test(argv):
    word = "Dreaming out loud"
    try:
        page = wikipedia.page(word)
        print(page.url)
        # file_content[i].append(page.url)
    except wikipedia.exceptions.DisambiguationError as e:
        print(e.options)
        for item in e.options:
            page = wikipedia.page(title=item)
            if lesk(page.content, word) == synset:
                print(page.url)
    except wikipedia.exceptions.PageError as e:
    #not a wikipedia page found
        file_content[i].append("")


def tdidf_test():
    text = """Singer, songwriter, and producer Ryan Tedder from the band OneRepublic has
    written hit songs for artists like Jennifer Lopez, Natasha Bedingfield, and
    Hillary Duff. Now, Ryan is recording hit songs with his band, OneRepublic, and
    their new album Dreaming Out Loud. VOA's Larry London met with Ryan Tedder when
    he toured the Washington area."""
    word = "Dreaming out loud"
    wikipedia_tfidf_extraction(word, text)

def main(argv):
    tdidf_test()
    # wiki_test(argv)



if __name__ == "__main__":
    main(sys.argv)
