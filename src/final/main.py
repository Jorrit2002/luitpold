#!/usr/bin/python3
"""
Wikifiyer by Jorrit Bakker & Robbert Stevens
"""
import tagging
import file_operations as fo
import sys
import math
from time import gmtime, strftime

def progress(i, total):
    """
    Prints i/total as a precentage
    """
    point = total / 100
    sys.stdout.write("\r {0:.2f}%".format((i / point)))
    sys.stdout.flush()

def main():
    starttime = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print(starttime)

    file_paths = fo.get_file_paths("en.tok.off")
    total = len(file_paths)
    i = 0

    # p_numbers = ["p41", "p49", "p51", "p57", "p63", "p64", "p74"]
    print("{} files".format(total))

    for path in file_paths:
        progress(i, total)
        print("\t{}".format(path))

        if "final/test" in path:
            i += 1
            continue


        file_content = fo.read_file(path)

        #                    0     1   2  3    4   5      6
        # A line is made of: begin end id word pos entity url
        # Create a the word list
        word_list = tagging.content_to_word_list(file_content)

        # Create the list with pos tags
        pos_word_list = tagging.pos_tags_to_content(word_list) #4

        nert_tagged_list = tagging.nert_to_content(word_list) #5


        # Create a dictionary with bigrams trigrams
        ngram_list = tagging.key_finder(word_list, nert_tagged_list)


        for line in range(len(file_content)):
            """Line is a number"""

            # add the pos to the file content line
            file_content[line].append(pos_word_list[line][1])

            # add the entity to the file content line
            file_content[line].append(nert_tagged_list[line][1])

            word = " ".join(ngram_list[line])
            wn_tag = tagging.wn_entity_to_content(word, word_list)
            if wn_tag is not None:
                file_content[line][5] = wn_tag

            if file_content[line][5] == "PERSON":
                file_content[line][5] = "PER"
            if file_content[line][5] == "ORGANIZATION":
                file_content[line][5] = "ORG"
            if file_content[line][5] == "LOCATION":
                file_content[line][5] = ""
            if file_content[line][5] == "O":
                file_content[line][5] = ""



            # add the wikipedia entry to the content line
            if file_content[line][5] != "": # has a entity tag
                if not len(ngram_list[line]) > 1:
                    if pos_word_list[line] in ["NN", "NNP", "NNPS", "NNS"]:
                        file_content[line].append(tagging.wikipedia_url(ngram_list[line], word_list))
                    else:
                        file_content[line].append("")
                else:
                    file_content[line].append(tagging.wikipedia_url(ngram_list[line], word_list))
            else:
                file_content[line].append("")

        # print([[row[0], row[1], row[2], row[3], row[4], row[5], row[6]] for row in file_content])
        fo.create_file(path+".pos.output",[[row[0], row[1], row[2], row[3], row[4], row[5], row[6]] for row in file_content])

        i += 1 #progress

    endtime = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print(starttime)
    print(endtime)

    """
    1) Pos tagging
    2) Entity tagging, plak de entity categorie achter de woorden
        2.a) Lematize word
        2.b) Nert tagging, wordnet
        2.c) Lesk tagging, voor juist synset
    3) Wikipedia page, plak de url/urls achter de woorden[max. 5]
        3.a) zoek Wikipedia voor url,
        3.b) plak url achter alles

    4) eventueel html pagina genereren
    """
if __name__ == "__main__":
    main()
