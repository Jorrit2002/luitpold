import sys
import src.week2.exercise_one as e_one
import src.week2.exercise_two as e_two

def main(argv):
    e_one.exercise_one()
    e_two.exercise_two()

if __name__ == "__main__":
    main(sys.argv)
